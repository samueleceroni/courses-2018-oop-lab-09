package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ConcurrentGUI extends JFrame{

	private static final long serialVersionUID = 1L;
	final private JPanel panel = new JPanel();
	final private JLabel counterDisplay = new JLabel();
	final private JButton upButton = new JButton("UP");
	final private JButton downButton = new JButton("DOWN");
	final private JButton stopButton = new JButton("STOP");
	final private static double WIDTH_RATE = 0.2;
	final private static double HEIGHT_RATE = 0.1;
	/*
	 * Constructor
	 */
	public ConcurrentGUI() {
		super();
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		counterDisplay.setText("0");
		panel.add(counterDisplay);
		panel.add(upButton);
		panel.add(downButton);
		panel.add(stopButton);
		this.setContentPane(panel);
		this.setSize((int)(screenSize.getWidth()*WIDTH_RATE), (int)(screenSize.getHeight()*HEIGHT_RATE));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		/*
		 * Agents start
		 */
		final Agent agent = new Agent();
        new Thread(agent).start();
        /*
		 * Handlers
		 */
		stopButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				agent.stopCounting();
				upButton.setEnabled(false);
				downButton.setEnabled(false);
				stopButton.setEnabled(false);
				
			}
			
		});
		
		upButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				agent.goUp();
			}
			
		});
		
		downButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				agent.goDown();
			}
			
		});
	}
	/*
	 * Functions Implementations
	 */

	/*
	 * Agent implementations
	 */
	private class Agent implements Runnable{
		private volatile boolean stop = false;
		private volatile boolean isGoingUp = true;
		private int counter = 0;

		@Override
		public void run() {
			while(!this.stop) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            ConcurrentGUI.this.counterDisplay.setText(Integer.toString(counter));
                        }
                    });
					if(this.isGoingUp) {
						this.counter++;
					} else {
						this.counter--;
					}
                    Thread.sleep(100);
				} catch(InvocationTargetException | InterruptedException ex) {
					ex.printStackTrace();
					break;
				}
			}
			
		}
		
		private void stopCounting() {
			this.stop = true;
		}
		private void goUp() {
			this.isGoingUp = true;
		}
		private void goDown() {
			this.isGoingUp = false;
		}
	}

}
