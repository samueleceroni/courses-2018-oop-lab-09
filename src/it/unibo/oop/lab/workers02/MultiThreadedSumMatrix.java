package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a standard implementation of the calculation.
 * 
 * */

public final class MultiThreadedSumMatrix implements SumMatrix {

    private final int nthread;

    /**
     * 
     * @param nthread
     *            no. of thread performing the sum.
     */
    public MultiThreadedSumMatrix(final int nthread) {
        this.nthread = nthread;
    }

    private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startPosColumn;
        private final int startPosRow;
        private final int nColumns;
        private final int nRows;
        private double res;

        /**
         * Build a new worker.
         * 
         * @param list
         *            the list to sum
         * @param startpos
         *            the initial position for this worker
         * @param nelem
         *            the no. of elems to sum up for this worker
         */
        Worker(final double[][] matrix, final int startPosColumn, final int startPosRow, final int nColumns, final int nRows) {
            super();
            this.matrix = matrix;
            this.startPosColumn = startPosColumn;
            this.startPosRow = startPosRow;
            this.nColumns = nColumns;
            this.nRows = nRows;
        }

        @Override
        public void run() {
            /*System.out.println("Working from column position " + startPosColumn
                             + " to column position " + (startPosColumn + nColumns - 1)
                             + " from row position " + startPosRow + " to column position "
                             + (startPosRow + nRows - 1));*/
            for (int i = startPosColumn; i < matrix.length && i < startPosColumn + nColumns; i++) {
                for (int j = startPosRow; j < matrix[0].length && j < startPosRow + nRows; j++) {
                    this.res += this.matrix[i][j];
                }
            }
        }

        /**
         * Returns the result of summing up the integers within the list.
         * 
         * @return the sum of every element in the array
         */
        public double getResult() {
            return this.res;
        }

    }

    @Override
    public double sum(final double[][] matrix) {
        final int nColumns = matrix.length % nthread + matrix.length / nthread;
        final int nRows = matrix[0].length % nthread + matrix[0].length / nthread;
        /*
         * Build a list of workers
         */
        final List<Worker> workers = new ArrayList<>(nthread);
        for (int startCol = 0; startCol < matrix.length; startCol += nColumns) {
            for (int startRow = 0; startRow < matrix[0].length; startRow += nRows) {
                workers.add(new Worker(matrix, startCol, startRow, nColumns, nRows));
            }
        }
        /*
         * Start them
         */
        for (final Worker w: workers) {
            w.start();
        }
        /*
         * Wait for every one of them to finish. This operation is _way_ better done by
         * using barriers and latches, and the whole operation would be better done with
         * futures.
         */
        long sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        /*
         * Return the sum
         */
        return sum;
    }

}
